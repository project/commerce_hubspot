<?php

namespace Drupal\commerce_hubspot_engagement\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Engagement entity edit forms.
 *
 * @ingroup commerce_hubspot_engagement
 */
class EngagementForm extends ContentEntityForm {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    MessengerInterface $messenger,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL
  ) {
    parent::__construct(
      $entity_repository,
      $entity_type_bundle_info,
      $time
    );

    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Set the order ID for the engagement from the route.
    /** @var \Drupal\commerce_hubspot_engagement\Entity\EngagementInterface $engagement */
    $engagement = $this->entity;
    $order_id = $this->getRouteMatch()->getParameter('commerce_order');
    $engagement->set('order_id', $order_id);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    // Output a message depending on, if the engagement is new or not.
    $entity = $this->entity;
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus(
          $this->t(
            'Successfully created a new %label engagement.',
            ['%label' => $entity->bundle()]
          )
        );
        break;

      default:
        $this->messenger()->addStatus(
          $this->t(
            'Successfully saved the %label engagement.',
            ['%label' => $entity->bundle()]
          )
        );
    }
  }

}
